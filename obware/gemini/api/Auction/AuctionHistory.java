package com.obware.gemini.api.Auction;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestGet;

public class AuctionHistory extends BasicRequestGet {

    private static transient String url = "v1/auction/";

    private long since;
    private int limit_auction_results = 50;
    private boolean include_indicative = true;

    private AuctionHistory(String symbol, long since, int limit_auction_results, boolean include_indicative) {
        super(url + symbol + "/history");
        this.since = since;
        this.limit_auction_results = limit_auction_results;
        this.include_indicative = include_indicative;
    }

    private transient Result[] results;

    @Override
    protected void handleResult(String result) {
        this.results = new Gson().fromJson(result, Result[].class);
    }

    public Result[] getResults() {
        return results;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class AuctionHistoryBuilder {
        private long since;
        private int limit_auction_results = 50;
        private boolean include_indicative = true;
        private String symbol;

        public AuctionHistoryBuilder(String symbol) {
            this.symbol = symbol;
        }

        public AuctionHistoryBuilder setSince(long since) {
            this.since = since;
            return this;
        }

        public AuctionHistoryBuilder setLimitAuctionResults(int limit_auction_results) {
            this.limit_auction_results = limit_auction_results;
            return this;
        }

        public AuctionHistoryBuilder setIncludedIndicative(boolean include_indicative) {
            this.include_indicative = include_indicative;
            return this;
        }

        public AuctionHistory build() {
            return new AuctionHistory(symbol, since, limit_auction_results, include_indicative);
        }
    }

    public class Result {
        private long timestamp, timestampms;
        private int auction_id, eid;
        private String event_type, auction_result, auction_price, auction_quantity, highest_bid_price, lowest_ask_price;

        public long getTimestamp() {
            return timestamp;
        }

        public long getTimestampms() {
            return timestampms;
        }

        public int getAuction_id() {
            return auction_id;
        }

        public int getEid() {
            return eid;
        }

        public String getEvent_type() {
            return event_type;
        }

        public String getAuction_result() {
            return auction_result;
        }

        public String getAuction_price() {
            return auction_price;
        }

        public String getAuction_quantity() {
            return auction_quantity;
        }

        public String getHighest_bid_price() {
            return highest_bid_price;
        }

        public String getLowest_ask_price() {
            return lowest_ask_price;
        }
    }
}