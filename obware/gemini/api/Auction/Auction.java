package com.obware.gemini.api.Auction;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestGet;

public class Auction extends BasicRequestGet {

    private static transient String url = "v1/auction/";

    public Auction(String symbol) {
        super(url+symbol);
    }

    private transient Result result;

    @Override
    protected void handleResult(String result) {
        this.result = new Gson().fromJson(result, Result.class);
    }

    public Result getResult(){
        return this.result;
    }

    public class Result{
        private long closed_until_ms,last_auction_eid, next_update_ms,next_auction_ms;
        private String last_auction_price,last_auction_quantity,last_highest_bid_price, last_lowest_ask_price,
                most_recent_indicative_price, most_recent_indicative_quantity, most_recent_highest_bid_price,
                most_recent_lowest_ask_price;

        public long getClosed_until_ms() {
            return closed_until_ms;
        }

        public long getLast_auction_eid() {
            return last_auction_eid;
        }

        public long getNext_update_ms() {
            return next_update_ms;
        }

        public long getNext_auction_ms() {
            return next_auction_ms;
        }

        public String getLast_auction_price() {
            return last_auction_price;
        }

        public String getLast_auction_quantity() {
            return last_auction_quantity;
        }

        public String getLast_highest_bid_price() {
            return last_highest_bid_price;
        }

        public String getLast_lowest_ask_price() {
            return last_lowest_ask_price;
        }

        public String getMost_recent_indicative_price() {
            return most_recent_indicative_price;
        }

        public String getMost_recent_indicative_quantity() {
            return most_recent_indicative_quantity;
        }

        public String getMost_recent_highest_bid_price() {
            return most_recent_highest_bid_price;
        }

        public String getMost_recent_lowest_ask_price() {
            return most_recent_lowest_ask_price;
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
