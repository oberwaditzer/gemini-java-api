package com.obware.gemini.api.Exception;

public class GeminiException extends Exception {

    public GeminiException() {
    }

    public GeminiException(String message) {
        super(message);
    }

    public GeminiException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeminiException(Throwable cause) {
        super(cause);
    }

    public GeminiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public class GeminiExceptionInformation {
        private String result, reason, message;


        public String getResult() {
            return result;
        }

        public String getReason() {
            return reason;
        }

        public String getMessage() {
            return message;
        }
    }
}
