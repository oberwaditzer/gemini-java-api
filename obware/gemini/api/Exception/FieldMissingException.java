package com.obware.gemini.api.Exception;

/**
 * Created by leono on 27.07.2017.
 */
public class FieldMissingException extends Exception {

    public FieldMissingException() {
    }

    public FieldMissingException(String message) {
        super(message);
    }

    public FieldMissingException(String message, Throwable cause) {
        super(message, cause);
    }

    public FieldMissingException(Throwable cause) {
        super(cause);
    }

    public FieldMissingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
