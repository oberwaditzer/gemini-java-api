package com.obware.gemini.api.Nonce;

import java.io.*;
import java.util.Scanner;

/**
 * Created by leono on 27.07.2017.
 */
public class Nonce {



    public static int getNonce(){
        try {
            Scanner scanner = new Scanner(new FileReader("nonce.txt"));
            StringBuilder stringBuilder = new StringBuilder();
            while(scanner.hasNext()){
                stringBuilder.append(scanner.next());
            }
            scanner.close();
            return Integer.parseInt(stringBuilder.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static void saveNonce(int nonce){
        try {
            Writer writer = new FileWriter("nonce.txt");
            writer.write(Integer.toString(nonce));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int increaseNonce(){
        int nonce = getNonce();
        saveNonce(nonce+1);
        return nonce+1;
    }

}
