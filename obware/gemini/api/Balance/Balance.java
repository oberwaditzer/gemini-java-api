package com.obware.gemini.api.Balance;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestPost;

/**
 * Created by leono on 26.07.2017.
 */
public class Balance extends BasicRequestPost {

    private transient static final String type = "/v1/balances";
    private transient static final String url = "v1/balances";

    private transient Result[] results;

    public Balance(int nonce) {
        super(url, type, nonce);
    }

    @Override
    protected void handleResult(String result) {
        this.results = new Gson().fromJson(result, Result[].class);
    }

    public Result[] balanceResults() {
        return this.results;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }


    public class Result {

        private String type, currency, amount, available, availableForWithdrawal;

        public String getType() {
            return type;
        }

        public String getCurrency() {
            return currency;
        }

        public String getAmount() {
            return amount;
        }

        public String getAvailable() {
            return available;
        }

        public String getAvailableForWithdrawal() {
            return availableForWithdrawal;
        }
    }
}
