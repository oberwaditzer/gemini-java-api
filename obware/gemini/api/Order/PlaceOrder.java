package com.obware.gemini.api.Order;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestPost;
import com.obware.gemini.api.Exception.FieldMissingException;

/**
 * Created by leono on 27.07.2017.
 */
public class PlaceOrder extends BasicRequestPost {

    private transient static String url = "v1/order/new";
    private transient static String request = "/v1/order/new";

    private String client_order_id;
    private String symbol;
    private String amount;
    private String price;
    private String side;
    private String type;
    private String[] options;

    private Order result;

    private PlaceOrder(int nonce, String client_order_id, String symbol, String amount, String price, String side, String type, String[] options) {
        super(url, request, nonce);
        this.client_order_id = client_order_id;
        this.symbol = symbol;
        this.amount = amount;
        this.price = price;
        this.side = side;
        this.type = type;
        this.options = options;
    }

    public Order getResult() {
        return this.result;
    }

    @Override
    protected void handleResult(String result) {
        this.result = new Gson().fromJson(result, Order.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class PlaceOrderBuilder {

        public static final String BUY = "buy";
        public static final String SELL = "SELL";
        public static final String EXCHANGE_LIMIT = "exchange limit";
        public static final String MAKER_OR_CANCEL = "maker-or-cancel";
        public static final String IMMEDIATE_OR_CANCEL = "immediate-or-cancel";

        private int nonce;
        private String client_order_id;
        private String symbol;
        private String amount;
        private String price;
        private String side;
        private String type;
        private String[] options;

        public PlaceOrderBuilder(int nonce) {
            this.nonce = nonce;
        }

        public PlaceOrderBuilder setSymbol(String symbol) {
            this.symbol = symbol;
            return this;
        }

        public PlaceOrderBuilder setClient_order_id(String client_order_id) {
            this.client_order_id = client_order_id;
            return this;
        }

        public PlaceOrderBuilder setAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public PlaceOrderBuilder setPrice(String price) {
            this.price = price;
            return this;
        }

        public PlaceOrderBuilder setSide(String side) {
            this.side = side;
            return this;
        }

        public PlaceOrderBuilder setType(String type) {
            this.type = type;
            return this;
        }

        public PlaceOrderBuilder setOptions(String[] options) {
            this.options = options;
            return this;
        }

        public PlaceOrderBuilder addOption(String option) {
            if (this.options == null || this.options.length == 0) {
                options = new String[1];
                options[0] = option;
            } else {
                String[] temp = new String[options.length + 1];
                for (int i = 0; i < temp.length; i++) {
                    temp[i] = options[i];
                }
                temp[client_order_id.length()] = option;
                this.options = temp;
            }
            return this;
        }

        public PlaceOrder build() throws FieldMissingException {
            if (client_order_id == null || symbol == null || amount == null || price == null || side == null || type == null) {
                throw new FieldMissingException("At least one field is missing");
            }
            return new PlaceOrder(nonce, client_order_id, symbol, amount, price, side, type, options);
        }
    }

}
