package com.obware.gemini.api.Order;

public class Order {

    private String order_id, client_order_id, symbol, price, avg_execution_price, side,
            type, timestamp, executed_amount, remaining_amount, original_amount;
    private long timestampms;
    private String[] options;
    private boolean is_live, is_cancelled;


    public String getOrder_id() {
        return order_id;
    }

    public String getClient_order_id() {
        return client_order_id;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getPrice() {
        return price;
    }

    public String getAvg_execution_price() {
        return avg_execution_price;
    }

    public String getSide() {
        return side;
    }

    public String getType() {
        return type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getExecuted_amount() {
        return executed_amount;
    }

    public String getRemaining_amount() {
        return remaining_amount;
    }

    public String getOriginal_amount() {
        return original_amount;
    }

    public long getTimestampms() {
        return timestampms;
    }

    public boolean isIs_live() {
        return is_live;
    }

    public boolean isIs_cancelled() {
        return is_cancelled;
    }

    public String[] getOptions() {
        return options;
    }
}
