package com.obware.gemini.api.Order;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestPost;

public class ActiveOrder extends BasicRequestPost{

    private transient static String url = "v1/orders";
    private transient static String request = "/v1/orders";

    private Order[] results;

    public ActiveOrder(int nonce) {
        super(url, request, nonce);
    }

    public Order[] orders(){
        return this.results;
    }

    @Override
    protected void handleResult(String result) {
        this.results = new Gson().fromJson(result, Order[].class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
