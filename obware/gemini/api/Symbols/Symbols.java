package com.obware.gemini.api.Symbols;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.obware.gemini.api.Basic.BasicRequestGet;

import java.lang.reflect.Type;
import java.util.List;

public class Symbols extends BasicRequestGet {

    private transient Result result;
    private static transient String url = "v1/symbols";

    public Symbols() {
        super(url);
    }

    public Result getResult() {
        return result;
    }

    @Override
    protected void handleResult(String result) {
        Type listType = new TypeToken<List<String>>() {
        }.getType();
        List<String> list = new Gson().fromJson(result, listType);
        this.result = new Result(list);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public class Result {
        private String[] symbols;

        private Result(List<String> symbols) {
            this.symbols = symbols.toArray(new String[symbols.size()]);
        }

        public String[] getSymbols() {
            return symbols;
        }
    }
}
