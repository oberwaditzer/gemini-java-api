package com.obware.gemini.api.OrderBook;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestGet;

public class OrderBook extends BasicRequestGet {

    private static transient String url = "v1/book/";
    private int limit_bids, limit_asks;

    private Result result;

    private OrderBook(String symbol, int limit_bids, int limit_asks) {
        super(url + symbol);
    }

    @Override
    protected void handleResult(String result) {
        this.result = new Gson().fromJson(result, Result.class);
    }

    public Result getResult() {
        return result;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class OrderBookBuilder {
        private int limit_bids, limit_asks;
        private String symbols;

        public static final int NO_LIMIT_BIDS = 0;
        public static final int NO_LIMIT_ASKS = 0;

        public OrderBookBuilder(String symbol){
            this.symbols = symbol;
        }

        public OrderBookBuilder setLimit_bids(int limit_bids){
            this.limit_bids = limit_bids;
            return this;
        }

        public OrderBookBuilder setLimit_asks(int limit_asks){
            this.limit_asks = limit_asks;
            return this;
        }

        public OrderBook build(){
            return new OrderBook(symbols, limit_bids, limit_asks);
        }


    }

    public class Result {
        Bids[] bids;
        Asks[] asks;


        public Bids[] getBids() {
            return bids;
        }

        public Asks[] getAsks() {
            return asks;
        }
    }

    public class Bids {
        String price, amount;

        public String getPrice() {
            return price;
        }

        public String getAmount() {
            return amount;
        }
    }

    public class Asks {
        String price, amount;

        public String getPrice() {
            return price;
        }

        public String getAmount() {
            return amount;
        }
    }
}
