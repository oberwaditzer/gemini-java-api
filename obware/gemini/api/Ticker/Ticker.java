package com.obware.gemini.api.Ticker;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestGet;

/**
 * Created by leono on 26.07.2017.
 */
public class Ticker extends BasicRequestGet {

    private transient static final String url = "v1/pubticker/";

    private transient Result result;

    public Ticker(String symbol) {
        super(url+symbol);
    }

    @Override
    protected void handleResult(String result) {
        this.result = new Gson().fromJson(result, Result.class);
    }

    @Override
    public String toString() {
        return null;
    }

    public Result getResult() {
        return result;
    }

    public class Result {
        String bid, ask;

        public String getBid() {
            return bid;
        }

        public String getAsk() {
            return ask;
        }
    }

}
