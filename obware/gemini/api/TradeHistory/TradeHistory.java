package com.obware.gemini.api.TradeHistory;

import com.google.gson.Gson;
import com.obware.gemini.api.Basic.BasicRequestGet;

public class TradeHistory extends BasicRequestGet {

    private static transient final String url = "v1/trades/";
    private long timestamp;
    private int limit_trades = 50;
    boolean include_breaks = false;

    private TradeHistory(String symbol, long timestamp, int limit_trades, boolean include_breaks) {
        super(url + symbol);
        this.timestamp = timestamp;
        this.limit_trades = limit_trades;
        this.include_breaks = include_breaks;
    }

    private transient Result[] results;

    public Result[] getResults() {
        return this.results;
    }

    @Override
    protected void handleResult(String result) {
        this.results = new Gson().fromJson(result, Result[].class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class TradeHistoryBuilder {
        private String symbol;
        private long timestamp;
        private int limit_trades = 50;
        boolean include_breaks = false;

        public TradeHistoryBuilder(String symbol) {
            this.symbol = symbol;
        }

        public TradeHistoryBuilder setTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public TradeHistoryBuilder setLimitTrades(int limit_trades) {
            this.limit_trades = limit_trades;
            return this;
        }

        public TradeHistoryBuilder setIncludeBreaks(boolean include_breaks) {
            this.include_breaks = include_breaks;
            return this;
        }

        public TradeHistory build() {
            return new TradeHistory(symbol, timestamp, limit_trades, include_breaks);
        }
    }

    public static class Result {
        private long timestamp, timestampms, tid;
        private String price, amount, exchange, type;

        public long getTimestamp() {
            return timestamp;
        }

        public long getTimestampms() {
            return timestampms;
        }

        public long getTid() {
            return tid;
        }

        public String getPrice() {
            return price;
        }

        public String getAmount() {
            return amount;
        }

        public String getExchange() {
            return exchange;
        }

        public String getType() {
            return type;
        }
    }
}
