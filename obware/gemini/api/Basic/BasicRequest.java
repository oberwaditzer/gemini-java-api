package com.obware.gemini.api.Basic;

import com.obware.gemini.api.Exception.GeminiException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

/**
 * Created by leono on 27.07.2017.
 */
public abstract class BasicRequest {

    private transient String key = "VNOh9vdsRHAXMXi4uN6M";
    private transient String secret = "t5fAXwbxc76XPAe3GVz1oGwyt9c";

    private static transient String basicUrl = "https://api.sandbox.gemini.com/";

    private transient String url;
    protected transient String response;

    public BasicRequest(String url) {
        this.url = this.basicUrl + url;
    }

    transient ResponseHandler<String> responseHandler = response -> {
        int status = response.getStatusLine().getStatusCode();
        if (status == 200) {
            HttpEntity entity = response.getEntity();
            return entity != null ? EntityUtils.toString(entity) : null;
        } else {
            HttpEntity entity = response.getEntity();
            String in = entity != null ? EntityUtils.toString(entity) : null;
            throw new ClientProtocolExceptionWithInformation("Unexpected response status: " + status, in);
        }
    };


    public String getKey() {
        return key;
    }

    public String getSecret() {
        return secret;
    }

    public String getUrl() {
        return url;
    }

    public abstract String get() throws GeminiException;

    protected abstract void handleResult(String result);

    String getBase64(String s) {
        return new String(Base64.encodeBase64(s.getBytes()));
    }

    String Hmac384(String key, String input) {
        return HmacUtils.hmacSha384Hex(key, input);
    }

    public static void setBasicUrl(String basicUrl) {
        BasicRequest.basicUrl = basicUrl;
    }

    @Override
    public abstract String toString();
}
