package com.obware.gemini.api.Basic;

import org.apache.http.client.ClientProtocolException;

public class ClientProtocolExceptionWithInformation extends ClientProtocolException {

    private String response;

    public ClientProtocolExceptionWithInformation() {
    }

    public ClientProtocolExceptionWithInformation(String s, String response) {
        super(s);
        this.response = response;
    }

    public ClientProtocolExceptionWithInformation(Throwable cause) {
        super(cause);
    }

    public ClientProtocolExceptionWithInformation(String message, Throwable cause) {
        super(message, cause);
    }

    public String getResponse() {
        return response;
    }
}