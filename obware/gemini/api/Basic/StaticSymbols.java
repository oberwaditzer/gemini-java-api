package com.obware.gemini.api.Basic;

public enum StaticSymbols {

    Bitcoin("Bitcoin"),
    Ethereum("Ethereum"),
    BitcoinDollar("btcusd"),
    EthereumDollar("ethusd"),
    EthereumBitcoin("ethbtc");

    private final String name;

    private StaticSymbols(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
