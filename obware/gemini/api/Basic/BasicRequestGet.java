package com.obware.gemini.api.Basic;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by leono on 27.07.2017.
 */
public abstract class BasicRequestGet extends BasicRequest {


    public BasicRequestGet(String url) {
        super(url);
    }

    @Override
    public String get() {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            String yourJson = this.toString();
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(yourJson);
            JsonObject obj = element.getAsJsonObject(); //since you know it's a JsonObject
            Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();//will return members of your object
            String urlToAdd = "";
            int count = 0;
            for (Map.Entry<String, JsonElement> entry : entries) {
                if (count == 0) {
                    urlToAdd += entry.getKey() + "=" + entry.getValue();
                } else {
                    urlToAdd += "&" + entry.getKey() + "=" + entry.getValue();
                }
                count++;
            }
            HttpGet httpGet = new HttpGet(getUrl() + ((urlToAdd == "") ? "" : "?" + urlToAdd));

            response = httpClient.execute(httpGet, responseHandler);
            handleResult(response);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
