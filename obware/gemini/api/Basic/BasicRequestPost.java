package com.obware.gemini.api.Basic;

import com.google.gson.Gson;
import com.obware.gemini.api.Exception.GeminiException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;

import java.io.IOException;

/**
 * Created by leono on 26.07.2017.
 */
public abstract class BasicRequestPost extends BasicRequest {

    private transient String key = "LRTRlFrrU2NERkf0B3p8";
    private transient String secret = "3NxGwKoTMD1Bqbvegkisbz9SBsGN";

    private String request;
    private int nonce;

    public BasicRequestPost(String url, String request, int nonce) {
        super(url);
        this.request = request;
        this.nonce = nonce;
    }

    public String get() throws GeminiException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpPost httpPost = new HttpPost(getUrl());
            httpPost.addHeader(new BasicHeader("X-GEMINI-APIKEY", key));
            httpPost.addHeader(new BasicHeader("X-GEMINI-PAYLOAD", getBase64(this.toString())));
            httpPost.addHeader(new BasicHeader("X-GEMINI-SIGNATURE", Hmac384(secret, getBase64(this.toString()))));

            response = httpClient.execute(httpPost, responseHandler);
            handleResult(response);
            return response;
        } catch (ClientProtocolException e) {
            ClientProtocolExceptionWithInformation clientProtocolExceptionWithInformation = (ClientProtocolExceptionWithInformation) e;
            GeminiException.GeminiExceptionInformation geminiExceptionInformation =
                    new Gson().fromJson(clientProtocolExceptionWithInformation.getResponse(), GeminiException.GeminiExceptionInformation.class);
            System.out.println(clientProtocolExceptionWithInformation.getResponse());
            throw new GeminiException(geminiExceptionInformation.getReason() + ". Reason: " + geminiExceptionInformation.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
