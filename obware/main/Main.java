package com.obware.main;

import com.obware.gemini.api.Auction.Auction;
import com.obware.gemini.api.Auction.AuctionHistory;
import com.obware.gemini.api.Basic.StaticSymbols;
import com.obware.gemini.api.Exception.GeminiException;
import com.obware.gemini.api.Nonce.Nonce;
import com.obware.gemini.api.Order.ActiveOrder;
import com.obware.gemini.api.Order.Order;
import com.obware.gemini.api.OrderBook.OrderBook;
import com.obware.gemini.api.Symbols.Symbols;
import com.obware.gemini.api.Ticker.Ticker;
import com.obware.gemini.api.TradeHistory.TradeHistory;

/**
 * Created by leono on 25.07.2017.
 */
public class Main {

    public static void main(String[] args) {


       /* Symbols symbols = new Symbols();
        symbols.get();
        String[] res = symbols.getResult().getSymbols();
        for(String s : res){
            System.out.println(s);
        }

        OrderBook orderBook = new OrderBook.OrderBookBuilder(res[0]).build();
        orderBook.get();
        OrderBook.Result result = orderBook.getResult();
        for(OrderBook.Asks ask : result.getAsks()){
            System.out.println(ask.getAmount());
            System.out.println(ask.getPrice());
        }

        Ticker ticker = new Ticker("btcusd");
        ticker.get();
        double price = Double.parseDouble(ticker.getResult().getBid());
        System.out.printf("ASK : " + ticker.getResult().getAsk());
        System.out.println("BID : " + price);*/
        try {
            ActiveOrder activeOrder = new ActiveOrder(Nonce.increaseNonce());
            activeOrder.get();
            Order[] orders = activeOrder.orders();
            for(Order order : orders){
                System.out.println(order.getPrice());
            }
            /*PlaceOrder placeOrder = new PlaceOrder.PlaceOrderBuilder(Nonce.increaseNonce()).setClient_order_id("1")
                    .setSymbol("btcusd")
                    .setAmount("1")
                    .setPrice(Double.toString(price))
                    .setSide(PlaceOrder.PlaceOrderBuilder.SELL)
                    .setType(PlaceOrder.PlaceOrderBuilder.EXCHANGE_LIMIT)
                    .addOption(PlaceOrder.PlaceOrderBuilder.IMMEDIATE_OR_CANCEL)
                    .build();
            String s = placeOrder.get();
            Order result = placeOrder.getResult();
            System.out.println("Is Cancelled : " + result.isIs_cancelled());
            System.out.println("Is Live : " + result.isIs_live());
            System.out.println("----");
            System.out.println(s);*/
            /*Balance balance = new Balance(Nonce.increaseNonce());
            balance.get();
            Balance.Result[] balanceResults = balance.balanceResults();
            for (Balance.Result result : balanceResults) {
                System.out.println(result.getAmount());
            }*/
        } catch (GeminiException e) {
            e.printStackTrace();
        }

        /*Balance balance = new Balance(9);
        System.out.println(balance.get());
        Balance.Result[] balanceResults = balance.balanceResults();
        for(Balance.Result result : balanceResults){
            System.out.println(result.getAmount());
        }
        Ticker ticker = new Ticker("ethusd");
        ticker.get();
        System.out.println(ticker.getResult().getAsk());
        System.out.println(ticker.getResult().getBid());*/

    }
}
